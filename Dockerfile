FROM python:3.8-alpine

ENV PATH="/scripts:${PATH}"

COPY ./requirements.txt /requirements.txt

RUN apk add --no-cache postgresql-libs && apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && python3 -m pip install -r requirements.txt --no-cache-dir && apk --purge del .build-deps

RUN mkdir /app
WORKDIR /app
COPY ./scripts /scripts

RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir

COPY /app /app/

RUN adduser -D user
RUN chown -R user:user /vol
RUN chmod -R 775 /vol/web

USER user
CMD ["entrypoint.sh"]
